FROM python
COPY . /app
RUN pip install --no-cache-dir -r requirements.txt 
EXPOSE 5000
WORKDIR /app
ENTRYPOINT [ "python" ]
CMD ["create_html_page.py"]

